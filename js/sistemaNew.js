var data = new Date;
var anoAtual = data.getFullYear();
var mesAtual = data.getMonth();
var diaAtual = data.getDate();
var feitas = 0;
var id = 0;
var alarm = true;
var music = true;
var bt = ['Desativo','Ativo'];
var DB = {
	'dados': [],
	'ipP': 'http://192.168.0.',
	'ip': '',
	'select': function(inf){
		var val = true;
		for (var i = 0; i < this.dados.length; i++) {
			var loc = this.dados[i];
			var comp = loc['hora']+loc['nome']+loc['minuto'];

			if (btoa(inf)==btoa(comp)) val = false;
		}
		
		return val;
	},
	'post': function(n,h,m){
		if(this.select(h+n+m) && n != "undefined" && n != ''){this.get(n,h,m);}
	},
	'get': function(n,h,m){
		this.dados.push({'nome': n,'hora': parseInt(h),'minuto': parseInt(m)});
	},
	'ini': function(){
		for (var i = 0; i < localStorage.length; i++) {
			var cache = localStorage.key(i).split(":");
			if(cache[2]=="1")
				this.post(localStorage.getItem(localStorage.key(i)),cache[0],cache[1]);
		}
	},
	'comp': function(){
		for (var i = 0; i < agendaLoc.length; i++) {
			var ag = agendaLoc[i];
			for (var e = 0; e < this.dados.length; e++) {
				var bg = this.dados[e];
				if(JSON.stringify(ag)==JSON.stringify(bg)) this.dados.splice(e,1);
			}
		}
	},	
	'auto': function(i){
			var loc = this.ipP+i+'/agenda.php';
			var formulario = new FormData();
			formulario.append('ping', 1);

			var requisicao = new XMLHttpRequest();
			requisicao.open('post',loc);

			if(!requisicao.send(formulario)){
				requisicao.onload = function (e) {
					if (requisicao.readyState === 4) {
						if (requisicao.status === 200) {
							if(requisicao.responseText.length > 1){
								console.log('%c Destino: '+loc,'color:#5cb85c;background-color:#fff;');
								DB.ip = loc;
								postInt("Banco de Dados","success","Servidor Encontrado!","ID: "+i);
								localStorage.setItem("ip",loc);
							}
						}
					}
				};
				requisicao.onerror = function (e) {
					console.log('%c Falha '+loc,'color:#f2dede;background-color:#a94442;');
				};
			}
	},
	'pesquisa': function(){
		for (var i = 100; i < 120; i++) {
			if (DB.ip.length > 0) break;
			else DB.auto(i);
		}
	},
	'up': function(){
		var resp = [];
		var loc = (DB.ip > 0)? DB.ip : document.querySelector("#C4").value;

		var formulario = new FormData();
		formulario.append('agenda', agendaLoc);

		var requisicao = new XMLHttpRequest();
		requisicao.open('post',loc);

		if(!requisicao.send(formulario)){
			requisicao.onload = function (e) {
			  if (requisicao.readyState === 4) {
			    if (requisicao.status === 200) {
			    	if(requisicao.responseText.indexOf(",")>-1){
			    		var conj = requisicao.responseText.split(",");
			    		for (var i = 0; i < conj.length; i++) {
			    			var atv = conj[i].split(":");
			    			if (atv[0].length>1)
			    				addAgenda(atv[0],atv[2],atv[1]);
			    		}
			    		console.log('%c Coletados de Dados: Sucesso','color:#fff;background-color:#5cb85c;');
			    	}else{
			    		var atv = requisicao.responseText.split(":");
						addAgenda(atv[0],atv[2],atv[1]);
			    	}
			    	org();
			    }
			  }
			};
		}
		requisicao.onerror = function (e) {
			console.log('%c Falha na Integração','color:#31708f;background-color:#d9edf7;');
		  	id++;
			postInt("Banco de Dados","danger","Falha na conexão!","ID: DB"+id);
			if(id>9){
				postInt("Banco de Dados","warning","Conexão desativada!","ID: Er"+id);
				seting(3);
				id = 0;
				console.log('%c Falha na Integração: DESLIGANDO','color:#fff;background-color:#ec971f;');
			}
		};
	}
};

if(!localStorage.getItem("conf"))
	localStorage.setItem("conf","1:1:0:1");

function atual() {
	var off = "btn btn-danger col-xs-6 disabled";
	if(document.querySelector('#C0').innerText=="Ativo" && modulo(0)==0) {
		document.querySelector('#C0').innerText = bt[0];
		document.querySelector("#C0").className = off;
	}
	if(document.querySelector('#C1').innerText=="Ativo" && modulo(1)==0) {
		document.querySelector('#C1').innerText = bt[0];
		document.querySelector("#C1").className = off;
	}
	if(document.querySelector('#C2').innerText=="Ativo" && modulo(2)==0) {
		document.querySelector('#C2').innerText = bt[0];
		document.querySelector("#C2").className = off;
	}
	if(document.querySelector('#C3').innerText=="Ativo" && modulo(3)==0) {
		document.querySelector('#C3').innerText = bt[0];
		document.querySelector("#C3").className = off;
	}

	window.requestAnimationFrame(atual);
}

function addAgenda(n,m,h) {
	var intro = true;
	for (var i = 0; i < agendaLoc.length; i++) {
		var db = agendaLoc[i];
		if(db['nome']==n && db['minuto']==m && db['hora']==h) intro = false;
	}
	if (intro){
		agendaLoc.push({'nome': n,'hora': h,'minuto': m});
		postInt("Banco de Dados","success","Nova Atividade!","ID: "+n);
	}
}

function seting(id) {
	var arrai = [1,1,0,1];
	if(id==2){
		localStorage.clear();
		location.reload();
	}	
	if(localStorage.getItem("conf")){
		arrai = localStorage.getItem("conf").split(":");
		arrai[id] = (parseInt(arrai[id])>0)? 0 : 1;
		localStorage.removeItem("conf");
		localStorage.setItem("conf",arrai[0]+':'+arrai[1]+':'+arrai[2]+':'+arrai[3]);
	}else{
		arrai[id] = (parseInt(arrai[id])>0)? 0 : 1;
		localStorage.removeItem("conf");
		localStorage.setItem("conf",arrai[0]+':'+arrai[1]+':'+arrai[2]+':'+arrai[3]);
	}				
	document.querySelector("#C"+id).innerText = bt[parseInt(modulo(id))];
	var css = (modulo(id)=='0')? "btn btn-danger col-xs-6 disabled" : "btn btn-success col-xs-6 disabled";
	document.querySelector("#C"+id).className = css;
}

function modulo(id) {
	if(localStorage.getItem("conf")){
		var modulo = localStorage.getItem("conf").split(":");
		return modulo[id];
	}
}

function diasNoMes(mes, ano) {
    var data = new Date(ano, mes, 0);
    return data.getDate();
}

function hora() {
	var Servidor = document.querySelector("#C4");
	var confg = localStorage.getItem("ip")? 2 : 1;
	var feito = Math.floor((feitas/(localStorage.length-confg)).toFixed(2)*100);
	if(parseInt(modulo(1)))
		document.querySelector('#dia').style = "width: "+feito+'%;';

	var agora = new Date;
	var segundo = (agora.getSeconds()<10)? '0'+agora.getSeconds() : agora.getSeconds();

	document.querySelector('#hora').innerText = formTime(agora.getHours(),agora.getMinutes())+":"+segundo;
	var base = (DB.ip.length > 0)? DB.ip : localStorage.getItem("ip")? localStorage.getItem("ip") : Servidor.value;
	if (base != Servidor.value) {
		Servidor.value = base;
	}
}

setInterval(function(){
	if (parseInt(modulo(3))) {
		DB.up();
	}
},10000);

function CriarForm() {
	for (var i = 1; i < 13; i++) {
		var check = (i==mesAtual+1)? "selected" : "";
		var v = i<10? 0+''+i : i;
		document.querySelector('#mesSet').innerHTML += '<option value="'+i+'" '+check+'>'+v+'</option>';
	}
	for (var i = 0; i < diasNoMes(mesAtual+1, anoAtual); i++) {
		var check = (i==diaAtual)? "selected" : "";
		var v = i<10? 0+''+i : i;
		document.querySelector('#diaSet').innerHTML += '<option value="'+i+'" '+check+'>'+v+'</option>';
	}
	for (var i = 0; i < 23; i++) {
		var check = (i==data.getHours())? "selected" : "";
		var v = i<10? 0+''+i : i;
		document.querySelector('#horaSet').innerHTML += '<option value="'+i+'" '+check+'>'+v+'</option>';
	}
	for (var i = 0; i < 60; i++) {
		var check = (i==data.getMinutes())? "selected" : "";
		var v = i<10? 0+''+i : i;
		document.querySelector('#minutosSet').innerHTML += '<option value="'+i+'" '+check+'>'+v+'</option>';
	}
}

function alarme() {
	setInterval(function(){
		var momento = new Date;

		var agora = momento.getHours()+":"+momento.getMinutes()+":1";
		if(localStorage.getItem(agora) && alarm){
			var nome = '#E'+momento.getHours()+''+momento.getMinutes();
			var valor = localStorage.getItem(agora);
			localStorage.removeItem(agora);

			setTimeout(function(){
				window.navigator.vibrate(2000);
				if(parseInt(modulo(0)))
					document.querySelector('#conf').play();
			},500);

			document.querySelector('#dados').innerText = '';
			if (document.querySelector(nome)){
				document.querySelector(nome).className = "ativo";
				alarm = false;		
				document.querySelector("#dados").innerText = valor+'.';
				if (document.querySelector("#dados").innerText){
					var tempo = formTime(momento.getHours(),momento.getMinutes());
					var men = "alertar('"+valor+"','"+tempo+"',true,'"+agora+"')";
					var men2 = "alertar('"+valor+"','"+tempo+"',false,'"+agora+"')";
					document.querySelector(".modal-footer").innerHTML = '<button type="button" class="btn btn-success" data-dismiss="modal" onclick="'+men+'">Certo</button><button type="button" class="btn btn-warning" data-dismiss="modal" onclick="'+men2+'">Fechar</button>';
					document.querySelector("#DP").click();
					if (valor){
						setTimeout(function(){
							for (var i = agendaLoc.length - 1; i >= 0; i--) {
								if(agendaLoc[i].hora == momento.getHours() && agendaLoc[i].minuto == momento.getMinutes()){
									agendaLoc.splice(i, 1);
								}
							}
						},2000);
						setTimeout(function(){
							if(document.body.className == "modal-open"){
								document.querySelector("#DP").click();
								postInt("Alerta de Atividade","warning","A atividade "+valor+" ocorreu: ",tempo);
								if (document.querySelector(nome))
									document.querySelector(nome).remove();
							}
						},4000);
					}
					document.querySelector(nome).remove();
				}
			}
		}
	},1000);
}

function criarEvento(getNome,getHora,getMinuto,idOR) {			
	var name = getHora+":"+getMinuto+":1";
	if (name.indexOf(" ")<0) {
		localStorage.setItem(name,getNome);
		desenharLinha(getNome,getHora,getMinuto,idOR);
	}
}

function alertar(texto,momento,estado,agora) {
	var result = agora.split(":");
	var hora = result[0];
	var minuto = result[1];

	if(texto){
		document.querySelector('#conf').pause()
		var est = (estado)? 'info' : 'warning';
		postInt("Alarme!",est,texto,momento);
		if (document.querySelector('#E'+hora+''+minuto[0]))
			document.querySelector('#E'+hora+''+minuto[0]).remove();
		if (localStorage.getItem(agora)) 
			localStorage.removeItem(agora);
		alarm = true;
	}
}

CriarForm();
setInterval(hora, 1000);
alarme();
atual();

function inicarAgenda() {
	DB.ini();
	DB.comp();
	var banco = DB.dados

	for (var i = 0; i < banco.length; i++) {
		agendaLoc.push(banco[i]);
	}

	agendaLoc.sort(function(a, b){return ((parseInt(a['hora']))+(a['minuto']/60))-((parseInt(b['hora']))+(b['minuto']/60))});

	for (var i = agendaLoc.length - 1; i >= 0; i--) {
		if(!document.querySelector('#E'+agendaLoc[i].hora+''+agendaLoc[i].minuto))
			criarEvento(agendaLoc[i].nome,agendaLoc[i].hora,agendaLoc[i].minuto,agendaLoc[i].hora+''+agendaLoc[i].minuto);
	}
}

function formTime(h,m) {
	h = (h<10)? 0+''+h : h;
	m = (m<10)? 0+''+m : m;
	return h+':'+m;
}

function desenharLinha(getNome,getHora,getMinuto,idOR) {
	var id = "'E"+idOR+"'";
	if(!document.querySelector('#E'+getHora+''+getMinuto)){
		var GetName = "mensagem('"+getNome+" às "+getHora+" horas e "+getMinuto+" minutos.','','')";

		var FormH = (getHora<10)? '0'+getHora : getHora;
		var FormM = (getMinuto<10)? '0'+getMinuto : getMinuto;

		var linha = '<div id='+id+'><label class="name" onclick="'+GetName+'"><input type="text" id="getNome" class="form-control" disabled value="'+getNome+'"></label><label class="inp" onclick="Del('+getHora+','+getMinuto+')"><input  class="form-control"  disabled value="'+FormH+'"></label><label class="inp"><input class="form-control" disabled value="'+FormM+'"></label></div>';

		var agora = new Date;
		var cookie = getHora+":"+getMinuto+":0";
		if(getHora < agora.getHours() || getHora == agora.getHours() && getMinuto < agora.getMinutes() || localStorage.getItem(cookie) == getNome){
			document.querySelector("#agendaF").innerHTML += linha;
			document.querySelector('#E'+getHora+''+getMinuto).className = "desativo";
			(localStorage.getItem(cookie) == getNome)? feitas : feitas++;
		}
		else
			document.querySelector("#agenda").innerHTML = linha+document.querySelector("#agenda").innerHTML;
	}
}

function Del(h, m) {
	var momento = new Date;
	var agora = h+":"+m+":1";
	if (localStorage.getItem(agora))
		mensagem("Apagar a atividade "+localStorage.getItem(agora),'drop('+h+','+m+')','');
}

function drop(h, m) {
	var momento = new Date;
	var agora = h+":"+m+":1";
		document.querySelector('#E'+h+''+m).remove();

	for (var i = agendaLoc.length - 1; i >= 0; i--) {
		if(agendaLoc[i].hora == h && agendaLoc[i].minuto == m){
			agendaLoc.splice(i, 1);
			if(feitas<localStorage.length){
				feitas++;
				var agora2 = h+":"+m+":0";
				localStorage.setItem(agora2,localStorage.getItem(agora));
				localStorage.removeItem(agora);
				postInt("Atividade Apagada","danger","A atividade "+localStorage.getItem(agora2)+" foi apagada",formTime(h,m));
			}
		}
	}
}

function mensagem(conteudo,men,men2) {
	var corpo = document.querySelector("#dados");
	corpo.innerHTML = conteudo;
	document.querySelector(".modal-header").className = (corpo.innerText.indexOf('Apagar')>-1)? "modal-header offSet" : "modal-header";
	document.querySelector(".modal-footer").innerHTML = '<button type="button" class="btn btn-success" data-dismiss="modal" onclick="'+men+'">Confirmar</button><button type="button" class="btn btn-warning" data-dismiss="modal" onclick="'+men2+'">Cancelar</button>';
	document.querySelector("#DP").click();
}

function postInt(nome,est,conteudo,mais) {
	var texto = '<div class="alert alert-dismissible alert-'+est+'"><button type="button" class="close" data-dismiss="alert">&times;</button><h4>'+nome+'</h4><p>'+conteudo+' <a href="#" class="alert-link">'+mais+'</a>.</p></div>';
	document.querySelector('#alertas').innerHTML = texto+document.querySelector('#alertas').innerHTML;
}

document.addEventListener('DOMContentLoaded',inicarAgenda);

document.addEventListener('click',function(){document.querySelector('#conf').pause();});
document.querySelector("#dia").onclick = function() {
	var feito = Math.floor((feitas/(localStorage.length-1)).toFixed(2)*100);
	mensagem("Seu dia está: "+feito+"% concluido!","","");
	postInt("Avanço do dia","success","Seu dia está: ",feito+"% concluido");
}
function org(){
	document.querySelector('#agenda').innerHTML = "";
	inicarAgenda();
}
function addAtv(setNome,setHora,setMinuto) {
	criarEvento(setNome,setHora,setMinuto,setHora+''+setMinuto);
	agendaLoc.push({'nome': setNome,'hora': setHora,'minuto': setMinuto});
	document.querySelector('#setNome').value = "";
	mensagem("Organizar as Atividades?","org()","");
}
document.querySelector('#Add').onclick = function(){
	var setNome = document.querySelector('#setNome').value;
	// var setMes = document.querySelector('#mesSet').value;
	// var setDia = document.querySelector('#diaSet').value;
	var setHora = document.querySelector('#horaSet').value;
	var setMinuto = document.querySelector('#minutosSet').value;

	if(localStorage.getItem(setHora+":"+setMinuto+":1"))
		mensagem("Já existe atividade nesse horário!<br>Deseja continuar?","addAtv('"+setNome+"',"+setHora+","+setMinuto+");","");
	else
		addAtv(setNome,setHora,setMinuto);
}